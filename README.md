# TUT thesis template

I tried to clean up most of the old TUT thesis template (`tutthesis.cls`) and
example document (was `d_tyo.tex`, now `kandi.tex`):

 * clean up
    * most of the "commenting as version control" notes
    * most of the "you can also do this ..." notes (refer to actual LaTeX guides
      instead)
 * go from iso-8859-1 with extras to utf-8
 * add uniform formatting (just tabs)
 * use consistent eol (unix)

Structural changes:

 * break off generic sections into files
 * disable sections not needed in B.Sc. `\if0 ... \fi`
 * use `\\jobname` prefixed naming scheme to differentiate template stuff from
   document stuff
    * to add a `kandi.tex` related figure, name it `kandi-figure.pdf`

## Getting it to compile

Current compilation result should be a simple document, no used refs, very
simple structure.

 1. Install [vagrant](https://www.vagrantup.com/)
 2. Install virtualbox
 3. Run `vagrant up`
 4. Wait for hours while whole internet is downloaded
 5. SSH into the virtual machine: `vagrant ssh`
 6. Move to the checkout directory: `cd /vagrant`

Then just `make` to have your working copy filled up with temporary TeX related
files. Apparently this is normal. On error, `xetex` like `pdflatex` will stop
and prompt you for a missing file or something like that. `q` or `X` for quit
usually works, `CTRL-C` might also.

Since the nature of using a lot of temporary files I've noticed that you should
always `make clean` before building a new pdf file, so basically I end using a
lot of `make clean all`. `make clean` should remove all of the temporary files.

### I don't want to use virtual machines...

... and I want to pollute my main operating system with gigabytes of packages I
forget to remove once I'm done with the current project.

Not recommended.

Look at the end of `Vagrantfile` for the current package installation instructions.
There is a single line that starts with `sudo apt-get install -y`, after that you
probably want to install most of the packages listed.

### Common problems

```
! I can't write on file `kandi.pdf'.
```

There is insufficient memory available. Quit chrome if you are using this on desktop
or give the virtual machine more RAM.

## Workflow with vagrant

 1. Edit from host operating system
 2. Compile in virtual machine
 3. Preview in host operating system
 4. Commit from host operating system

## Starting your own project

 1. Fork this repository as private fork
 2. Clone it locally
 3. `vagrant up`
 4. Get the kandi.pdf out, make sure it looks pretty ok
 5. Rename and edit files wildly
 6. Keep the commits on your own private fork

## Fixing common problems in this fork

Assuming you are following the workflow outlined above:

 1. Locate the fix you want to create pull request for
 2. Create a new branch from the latest commit in this repository
 3. Cherry-pick or re-implement your fix in this branch
 4. (Not sure if Bitbucket allows PR's from private forks)

Don't forget to go back to your own `master` branch afterwards: `git checkout master`.

## License

The original file from the university (tutthesis.cls) has a [LaTeX Project
Public License v1.3](https://latex-project.org/lppl/) reference, so all this
cleanup effort based on that is also licensed as the above.

The logos also contained might not fall into any open source license but are
© [Tampere University of Technology](https://www.tut.fi), they are only
present to fit into the TUT thesis guidelines and should not be used in any
other way.

Repository history might contain files related to the original example document
(sample figures, code and stuff like that) which most likely are not licensed
as the template is.

I of course might accidentially push my actual thesis work into this repository
before it's ready for publication. All rights reserved for that content.
