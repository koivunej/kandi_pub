TARGET=kandi

all: ${TARGET}.tex
	xelatex ${TARGET}.tex && \
	bibtex ${TARGET} && \
	xelatex ${TARGET}.tex && \
	xelatex ${TARGET}.tex

clean:
	rm -f *aux *dvi *log *toc *lof *lot *lol *bbl *blg *out log.make ${TARGET}-blx.bib ${TARGET}.run.xml ${TARGET}.pdf
